<?php
// Đường dẫn tới thư mục gốc chứa các ảnh
$rootDirectory = 'C:\Users\Admin\Downloads\T-Shirt\T-Shirt';
// Tách chuỗi thành mảng dựa trên dấu gạch chéo ngược
$pathParts = explode('\\', $rootDirectory);
// Lấy phần tử thứ 4 trong mảng (là dấu \ thứ 5 trong đường dẫn ban đầu)
$partialPath = $pathParts[4];
// Hàm để lấy SKU cho tên ảnh (Ví dụ: giả sử bạn có một hàm như này để lấy SKU từ tên ảnh)
function getSKUForImage($imageNames)
{
    // Viết mã để lấy SKU từ tên ảnh
    // Ví dụ: Giả sử tên ảnh có dạng "sku123.jpg", SKU sẽ là chuỗi "sku123"
    $sku = substr($imageNames, 0, strpos($imageNames, '.'));
    return $sku;
}
// Tạo tệp CSV để ghi thông tin
$csvFilePath = 'C:\Users\Admin\Downloads\T-Shirt\format.csv';
$csvFile = fopen($csvFilePath, 'w');

// Ghi tiêu đề cho tệp CSV
fputcsv($csvFile, ['Title', 'Image', 'Tag', 'Type', 'Price', 'Category1', 'Category2', 'Category3', 'Sku', 'File ID', 'Date Mock']);
// Hàm để duyệt qua các thư mục và tệp, lấy thông tin ảnh và ghi vào tệp CSV
function traverseDirectoriesAndWriteToCSV($directory, $csvFile)
{
    $directoryContent = scandir($directory);
    $imageNames = array();
    foreach ($directoryContent as $item) {
        // Bỏ qua các thư mục "." và ".."
        if ($item === '.' || $item === '..') {
            continue;
        }

        $path = $directory . '/' . $item;

        if (is_dir($path)) {
            // Nếu là thư mục, tiếp tục đệ quy để duyệt qua các thư mục con
            traverseDirectoriesAndWriteToCSV($path, $csvFile);
        } elseif (is_file($path) && pathinfo($path, PATHINFO_EXTENSION) === 'jpg' || pathinfo($path, PATHINFO_EXTENSION) === 'jpeg') {
            // Nếu là tệp ảnh JPG, ghi thông tin vào tệp CSV

            $folderName = basename(dirname($path));
            $files =  scandir($directory);
            // Lọc và lấy ra chỉ các tệp hình ảnh
            $imageFiles = array_filter($files, function ($file) {
                $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                $validExtensions = array('jpg', 'jpeg', 'png');
                return in_array($extension, $validExtensions);
            });
            // Lặp qua từng tệp hình ảnh và lưu đường dẫn vào mảng $imagePaths
            foreach ($imageFiles as $imageFile) {
                // Lưu đường dẫn vào mảng $imagePaths
                $imagePaths[] = $imageFile;
            }

            $imageNames[] = basename($path);

            // Giả sử bạn có một hàm để lấy SKU và giá trị cho cột SKU dựa vào tên ảnh
            $sku = '';
            // Điền giá trị còn lại cho các cột Title, Image, Tag, Type, Price, Category1, Category2, Category3, File ID, Date Mock
            $title = ''; // Điền giá trị tương ứng cho cột Title
            $tag = ''; // Điền giá trị tương ứng cho cột Tag
            $type = ''; // Điền giá trị tương ứng cho cột Type
            $price = ''; // Điền giá trị tương ứng cho cột Price
            $category1 = ''; // Điền giá trị tương ứng cho cột Category1
            $category2 = ''; // Điền giá trị tương ứng cho cột Category2
            $category3 = ''; // Điền giá trị tương ứng cho cột Category3
            $fileID = ''; // Điền giá trị tương ứng cho cột File ID
            $dateMock = ''; // Điền giá trị tương ứng cho cột Date Mock



        }
    }
    // Ghi thông tin vào tệp CSV
    fputcsv($csvFile, [$title, implode('|', $imageNames), $tag, $type, $price, $category1, $category2, $category3, $sku, $fileID, $dateMock]);
}





// Gọi hàm duyệt qua các thư mục và tệp và ghi thông tin vào tệp CSV
traverseDirectoriesAndWriteToCSV($rootDirectory, $csvFile);

// Đóng tệp CSV sau khi đã ghi xong
fclose($csvFile);

echo "CSV file has been generated with image information including SKU.";
